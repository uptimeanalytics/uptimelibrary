import pandas as pd
import sqlalchemy
from croniter import croniter
from datetime import datetime

class TestingData:
    def __init__(self, conn, variables, init_date, end_date):
        self.connection_string = conn
        self.variables = variables
        self.init_date = init_date
        self.end_date = end_date

    def get_data(self):
        """
        This method gets the variables on self.variables that are on the
        table `global_metric`.
        
        Returns: DataFrame - Raw data
        """
        labels = ", ".join(map(lambda e: "'" + e + "'", self.variables))
        engine = sqlalchemy.create_engine(self.connection_string)
        data = pd.read_sql("""
            SELECT time, value, label
            FROM
                global_measurement
            INNER JOIN
                global_metric
            ON global_metric.id = global_measurement.global_metric_id
            WHERE 
                global_metric_id IN (SELECT id FROM global_metric WHERE label IN ({labels}))
            AND time BETWEEN '{init_date}' AND '{end_date}'
        """.format(init_date=self.init_date, end_date=self.end_date, labels=labels), con=engine)
        return data

    def consult(self):
        """
        This method gets the variables on self.variables that are on the
        table `global_metric`. With the minimum transformations.

        Returns: DataFrame - Processed data        
        """
        data = self.get_data()
        data = data.pivot(index='time', columns='label')
        data.columns = data.columns.droplevel(level=0)
        return data

class Alert:
    """
    This class helps with the testing and development
    of alerts

    Args:
        - function: function - f(x) that returns true if the alarm
        was activated given the data.
        - name: str - Name of the alarm
        - data: The data for the simulation of the alert
        
    """

    def __init__(self, function, **kwargs):
        self.simulated_historical = kwargs.get('data', None)
        self.lambda_alert = function
        self.name = kwargs.get('name', None)

    def simulate(self, crontab, window):
        """
        This method simulates the functioning of the alarm 
        pass or define as a method.

        Args:
            - crontab: str - Crontab String that determines the frequency of the alarm
            - window: datetime.timedelta - The time window for the data. The start time for the data
            will be the time of the alarm call minus the window.
        """
        # Consult the data
        data = self.simulated_historical.consult()

        base = data.index[0] # First datetime
        end_date = data.index[-1] # Last datetime
        crontab_iterator = croniter(crontab, base)
        next_alarm_datetime = crontab_iterator.get_next(datetime)
        
        results = pd.DataFrame(columns=['time', 'name'])
        while next_alarm_datetime <= end_date:
            # Simulate the alarm
            start_of_window = next_alarm_datetime - window
            if self.lambda_alert(data[start_of_window:next_alarm_datetime]):
                results = results.append({'time': next_alarm_datetime, 'name': self.name}, ignore_index=True)
            next_alarm_datetime = crontab_iterator.get_next(datetime)
        
        return results