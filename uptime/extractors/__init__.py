import pandas as pd
import numpy as np
from tqdm import tqdm
from datetime import datetime
from sqlalchemy import create_engine
from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, NumberAttribute



class DynamoExtractor:
    def __init__(self, aws_key_id, aws_secret_key, region):
        self.aws_key_id = aws_key_id
        self.aws_secret_key = aws_secret_key
        self.region = region

    def _create_base_model(self, table):
        """
        Creates a PynamoDB Model only with date as its key.
        """
        self.table = table
        class BaseModel(Model):
            class Meta:
                aws_access_key_id = self.aws_key_id
                aws_secret_access_key = self.aws_secret_key
                table_name = table
                region = self.region
            date = UnicodeAttribute(hash_key=True)

        return BaseModel

    def _update_base_model(self, columns, base_model):
        """
        Update the PynamoDB model, adding the columns given on the list `columns`
        
        Args:
            - columns: list - List of the dictionaries of the style [{'column_name': 'type'}].
            - base_model: PynamoDB Model - Base model without the columns to be added.

        Returns:
            - PynamoDB Model - A subclass of the Base model added the columns.
        """
        for element in columns:
            name, value_type = list(element.items())[0]
            if name == 'date': continue
            if value_type == 'N':
                t = NumberAttribute()
                setattr(base_model, name, t)
                base_model._attributes[name] = t
            if value_type == 'S':
                t = UnicodeAttribute()
                setattr(base_model, name, t)
                base_model._attributes[name] = t

        class Model(base_model):
            class Meta:
                aws_access_key_id = base_model.Meta.aws_access_key_id
                aws_secret_access_key = base_model.Meta.aws_secret_access_key
                table_name = base_model.Meta.table_name
                region = base_model.Meta.region
        
        return Model

    def _get_columns_name(self, base_model, date):
        """
        Returns the columns base on the BaseModel
        Args:
            - base_model: Class to define a PynamoDB Model
            - date: Date of (hash_key) that uses to determine the columns
        Returns:
            List of dicts describing the name of the columns and types in the table.
            [{'name_1' : 'N'}, {'name_2': 'S'}, {'name_3': 'N'}, ...]

        Note: For specifics on the type consult DynamoDB documentation.
        """
        structure = base_model._get_connection().query(date, limit=1)['Items']
        if structure:
            structure = structure[0]
            structure = list(map(lambda e: {e[0]: list(e[1].items())[0][0]}, structure.items()))
            return structure
        return [{'time': 'S'}]
        


    def get_data(self, table, start_time=None, end_time=None, columns=None, rate_limit=None, page_size=None):
        """
        This method downloads the data from a given table from init_date to end_date.
        Args:
            - table: str - Name of the table
            - start_time: str - String of start time on isoformat
            - end_time: str - String of the end date on isoformat
            - columns: list - List of names of columns to ask.
            - rate_limit (optional): Limit to slowdown operation to conform to capacity limitations. Default None
            - page_size (optional): Number of items for each request. To limit the read capacity necessary.
        Return:
            - DataFrame containing the data
        """
        # Creates the correct model
        base_model = self._create_base_model(table)

        # Convert dates to datetime format
        start_time = datetime.fromisoformat(start_time)
        end_time = datetime.fromisoformat(end_time)
        
        
        data = []
        # Ranges of dates to ask by query
        ranges_of_dates = pd.date_range(start_time.date(), end_time.date())
        ranges_of_dates = list(map(lambda dt: dt.isoformat()[:10], ranges_of_dates))


        with tqdm() as pbar:
            for date in ranges_of_dates:
                # Reduce the number of columns
                columns_and_types = self._get_columns_name(base_model, date)
                
                # tuple(dict) -> tuple(keys)
                if columns:
                    columns = set(columns)
                    # Always add time
                    columns.add('time')
                    columns_and_types = filter(lambda e: tuple(e)[0] in columns, columns_and_types)

                model = self._update_base_model(columns_and_types, base_model)

                # In the start date and end date also ask for the time for limiting
                rate_options = {
                    'rate_limit' : rate_limit,
                    'page_size' : page_size
                }
                if date == start_time.date().isoformat()[:10] == end_time.date().isoformat()[:10]:
                    request = model.query(date, (model.time.between(start_time.time().isoformat(), end_time.time().isoformat())),
                        **rate_options)
                elif date == start_time.date().isoformat()[:10]:
                    request = model.query(date, model.time >= start_time.time().isoformat(), 
                        **rate_options)
                    print('start', start_time.time().isoformat())
                elif date == end_time.date().isoformat()[:10]:
                    request = model.query(date, model.time <= end_time.time().isoformat(),
                        **rate_options)
                    print('end', end_time.time().isoformat())
                else:
                    request = model.query(date, **rate_options)
                for entry in request:
                    data.append(entry)
                    pbar.update(1)
        # Transform the data
        data = map(lambda e: e.attribute_values, data)
        data = pd.DataFrame(data)
        return data

    def consult(self, table, init_date=None, end_date=None, columns=None, rate_limit=None, page_size=None):
        result = self.get_data(table, init_date, end_date, columns, rate_limit, page_size)
        result['_datetime'] = pd.to_datetime(result['date']+' '+ result['time'])
        result.set_index('_datetime', inplace=True)
        return result

# Constants Queries for CenitDB
virtual_table_for_custom_query = """
select 
    time_bucket_gapfill('1 sec', cross_time, '{start_time}', '{end_time}') AS time,
    1 as _1,
    {locf_columns}
    from 
    (
        SELECT * FROM crosstab(
            'select 
                time ,
                variable_name,
                value
            from sensor_data
            where variable_name in ({crosstab_variables})
            and time between ''{start_time}'' and ''{end_time}''
            order by 1',
            '{category_sql}'
            ) as (
                cross_time timestamp,
                {crosstab_columns}
            )
    ) as foo
GROUP BY time
"""

crosstab_query = """
select {option}
from crosstab(
    'select 
        time,
        variable_name,
        value
    from sensor_data
    where variable_name in ({crosstab_variables})
    and time between ''{start_time}'' and ''{end_time}''
    order by 1',
    '{category_sql}'
) as (
    time timestamp,
    {crosstab_columns}
);
"""


class CenitDB:
    def __init__(self, conn=None, local=False):
        '''
        Create CenitDB object
        Args:
            - conn : Postgres connection string
            - local: Boolean. When True creates object without connection.
        '''
        if not local:
            
            self.conn = conn
            self.engine = create_engine(conn)
            self.connection_time = datetime.now()
            self.category_sql = 'select distinct(variable_name) from sensor_data'
            variables = pd.read_sql(self.category_sql, con=self.engine)
            self.variables = variables['variable_name'].to_list()
            self.crosstab_columns = ",\n".join(variables['variable_name'].apply(lambda e: e + ' ' + 'double precision'))
            self.crosstab_variables = ", ".join(variables['variable_name'].apply(lambda e: "''" + e + "''"))
            self.locf_columns = ",\n".join(variables['variable_name'].apply(lambda e: 'locf(avg(' + e + '), treat_null_as_missing:=true) as ' + e))
            self.category_sql = 'SELECT * FROM (VALUES' + ', '.join(map(lambda e: "(''" + e + "'')", self.variables)) + ') as _'
            self.nulls = len(variables)
        

    def _reconnect(self):
        if abs((self.connection_time - datetime.now()).total_seconds()) > 180.0:
            self.engine = create_engine(self.conn)
            self.connection_time = datetime.now()
        return

    def get_data(self, start_time=None, end_time=None, chunksize=100000):
        """
        Get the data from the online database
        Args:
            - start_time : str. Initial time from necessary data on iso format
            - end_time: str. Final time of necessary data on iso format
            - chunksize: int. Equivalent to chunksize for pandas.DataFrame.read_sql method.s
        """
        self._reconnect()
        if not start_time:
            # Fill the start time
            start_time = pd.read_sql('SELECT min(time) from sensor_data', con=self.engine)['min'][0]
        else:
            # Check for date right before the date start
            right_before_start = pd.read_sql("""
                SELECT min(time) 
                FROM (
                    SELECT MAX(time) as time
                    FROM sensor_data
                    WHERE time < '{}'
                    GROUP BY variable_name
                ) pre_eval
            """.format(start_time), con=self.engine)
            # If there exists entries right before start_date use the maximum of that subset
            if not right_before_start.empty:
                start_time = right_before_start['min'][0]

        
        if not end_time:
            end_time = datetime.now().isoformat()
        options = {
            'start_time' : start_time,
            'end_time' : end_time,
            'crosstab_columns' : self.crosstab_columns,
            'crosstab_variables' : self.crosstab_variables,
            'category_sql' : self.category_sql
        }
        options['option'] = 'count(*)'
        total_data = pd.read_sql(crosstab_query.format(**options), con=self.engine)['count'][0]
        # Downloading data
        print('Downloading data...')
        options['option'] = '*'
        chunks = pd.read_sql(crosstab_query.format(**options), con=self.engine, chunksize=chunksize)
        data = pd.DataFrame()
        print('Joining data...')
        
        for chunk in tqdm(chunks, total=(total_data//chunksize + 1)):
            data = pd.concat([data, chunk])

        data['idx'] = data.reset_index().index
        self.data = data.set_index('time').fillna(np.nan)
        print('Done.')

    def save(self, path):
        if not hasattr(self, 'data'):
            print('There is not data. Please load or download data')
            return
        self.data.drop(['idx'], axis=1).to_csv(path)

    def load(self, path):
        data = pd.read_csv(path)
        data['idx'] = data.index
        data['time'] = pd.to_datetime(data['time'])
        data = data.set_index('time')
        self.data = data

    def consult(self, start_time, end_time):
        """
        Consult the data resampled second by second.
        """
        if not hasattr(self, 'data'):
            self.get_data(start_time=start_time, end_time=end_time)
        data_slice = self.data[start_time : end_time]['idx']
        fslice = self.data.resample('1S').mean().ffill().drop(['idx'], axis=1)[start_time: end_time]
        return fslice.reindex(pd.date_range(start_time, end_time, freq='1S'), fill_value=np.nan).ffill()
        

    def custom_query(self, sqltext, columns=None, start_time=None, end_time=None, chunksize=100000):
        """
        Consult directly to the server with
        Args:
            - sqltext: SQL consult where table must be {cenit_table}.
        """
        self._reconnect()
        if start_time:
            right_before_start = pd.read_sql("""
                SELECT min(time) 
                FROM (
                    SELECT MAX(time) as time
                    FROM sensor_data
                    WHERE time < '{}'
                    GROUP BY variable_name
                ) pre_eval
            """.format(start_time), con=self.engine)
            # If there exists entries right before start_date use the maximum of that subset
            if not right_before_start.empty:
                start_time = right_before_start['min'][0]
        else:
            start_time = pd.read_sql('SELECT min(time) from sensor_data', con=self.engine)['min'][0]
        if not end_time:
            end_time = datetime.now().isoformat()
        # Given columns calculate the parameters
        if not columns:
            columns = self.variables
    
        locf_columns = ",\n".join(map(lambda e: 'locf(avg(' + e + '), treat_null_as_missing:=true) as ' + e, columns))
        crosstab_variables = ", ".join(map(lambda e: "''" + e + "''", columns))
        crosstab_columns = ",\n".join(map(lambda e: e + ' ' + 'double precision', columns))
        category_sql = 'SELECT * FROM (VALUES' + ', '.join(map(lambda e: "(''" + e + "'')", columns)) + ') as _'
        nulls = len(columns)

        options = {
            'start_time' : start_time,
            'end_time' : end_time,
            'locf_columns' : locf_columns,
            'crosstab_columns' : crosstab_columns,
            'crosstab_variables' : crosstab_variables,
            'category_sql' : category_sql,
            'NULLS' : ", ".join(['NULL'] * nulls)
        }

        table = '(' + virtual_table_for_custom_query.format(**options) + ') as cenit_table'
        query = sqltext.format(cenit_table=table)
        print('Calculating and downloading data')
        chunks = pd.read_sql(query, con=self.engine, chunksize=chunksize)
        data = pd.DataFrame()
        for chunk in tqdm(chunks):
            data = pd.concat([data, chunk])
        
        return data