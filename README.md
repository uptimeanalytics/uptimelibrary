# Uptime Library

This repository is a library for the extraction, creation and deployment of various tools, given an easy to follow interface.

The focus of the library is on frequently, simple use cases where there is no necessity for complete specification of the standars on libraries.

## Installation

To install the database first clone this repository
```
git clone git@bitbucket.org:uptimeanalytics/uptimelibrary.git
```

Make sure you have insalled both Python and pip normally they come bundled together then proceed to the instalation

```
#Move to the dist folder in the repository
cd uptimelibrary/dist/

#Perform the installation using pip and the whl file
python -m pip install uptime-0.2.1-py3-none-any.whl

#Wait for the process to finish without any errors
```
You can now use the library normally by importing its classes.
## DynamoDB

Our database on dynamo is a NoSQL database, in this database each entry has an specific day and rest of the entry is part of the document. Because of this we can only ask for date truncated on days and not the whole timestamp.

```python
from uptime.extractors import DynamoExtractor

db = DynamoExtractor({aws_key_id}, {aws_secret_key}, {region})

# The init_date and end_date parameters are NOT optional
data = db.get_data({table_name}, {start_time}, {end_time})

# If you want to limit the throughput of your petition
# you can change two parameters to slow down the reading
# first the page size, that limits the amount of items on a request
data = db.get_data({table_name}, {start_time}, {end_time}, page_size=3)

# Then there is the rate limit, that tries to limit
# the request ask by the second.
data = db.get_data({table_name}, {start_time}, {end_time}, rate_limit=1)

# Reducing the page_size or the rate_limit will make the download
# go slower.


# You can also use the method consult to download the data and 
# directly have the index as the datetime formed by the columns
# date and time
data = db.consult({table_name}, {start_time}, {end_time})
# And you can slice by time
data[some_date : later_date]
```

## Cenit - Postgresql

```python
from uptime.extractors import CenitDB

conn = 'postgres://{username}:{password}@3{host}:{port}/{database}'

db = CenitDB(conn=conn)

# start_time and end_time can be whatever datetime on iso format.
# Download the raw data
db.get_data('2020-11-02', '2020-11-03')
data = db.consult('2020-11-02', '2020-11-03')
```

# Dev Documentation

To explain how to develop and add to the library, there needs to be a cleare understanding of the components build to this day:

## Extractors

### DynamoExtractor

`Class Dynamo Extractor`: This class connects with all dynamoDB tables using pynamoDB and the fact that the partition key is `date` and the ordering key is `time` without this keys the class is obsolete.

### CenitDB

`Class CenitDB`: This class connects to the PostgreSQL Database in the product for Cenit, this database has a log style of writing (datetime, column_name, value), but there is need for a transverse table with granularity second by second, this library makes possible this using a mix between crosstab (for transversing), locf (for filling data), and time_bucket_gapfill (for granularity).

And returns the last information constructed or carries out other SQL style consults for the user.

## Packaging

For packaging I create a .whl package for a pip installation (It seems the more natural), Here is tutorial: https://packaging.python.org/tutorials/packaging-projects/
